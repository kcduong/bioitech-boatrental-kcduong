/**
  database scheme
 */

DROP TABLE IF EXISTS boats;
DROP TABLE IF EXISTS boat_availabilty;






CREATE TABLE boats (
  id        INT(100) NOT NULL AUTO_INCREMENT,
  name      VARCHAR(40) NOT NULL,
  type      ENUM('MOTORBOAT', 'SAILBOAT', 'WATERSCOOTER'),
  price     INT(100) NOT NULL,
  PRIMARY KEY (id)

);

CREATE TABLE boat_availability(
  boat_name   INT NOT NULL,
  taken_day INT(31) NOT NULL,
  FOREIGN KEY (boat_name) REFERENCES boats(name)

);
