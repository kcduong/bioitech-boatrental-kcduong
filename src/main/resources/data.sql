/**
dummy data containing boat info
 */

INSERT INTO boats (name, type, price) VALUES
  ("Grape", "MOTORBOAT", 9),
  ("Gonzales", "MOTORBOAT", 9),
  ("Hermes", "MOTORBOAT", 9),
  ("Titanic", "SAILBOAT", 5),
  ("Sparrow", "SAILBOAT", 5),
  ("Ponyo", "WATERSCOOTER", 3);