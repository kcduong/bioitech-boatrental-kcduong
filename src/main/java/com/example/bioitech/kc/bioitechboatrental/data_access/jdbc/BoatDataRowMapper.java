package com.example.bioitech.kc.bioitechboatrental.data_access.jdbc;

import com.example.bioitech.kc.bioitechboatrental.model.Boat;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BoatDataRowMapper implements RowMapper<Boat>{
    @Override
    public Boat mapRow(ResultSet result, int rowNum) throws SQLException {
        Boat boat = new Boat();
        boat.setBoatName(result.getString("name"));
        boat.setBoatType(result.getString("type"));
        boat.setBoatPrice(result.getInt("price"));
        return boat;
    }
}
