package com.example.bioitech.kc.bioitechboatrental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class BioitechBoatRentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(BioitechBoatRentalApplication.class, args);
	}

}
