package com.example.bioitech.kc.bioitechboatrental.data_access.jdbc;


import com.example.bioitech.kc.bioitechboatrental.data_access.BoatDataSource;
import com.example.bioitech.kc.bioitechboatrental.model.Boat;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class BoatDataSourceJdbc implements BoatDataSource{

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public BoatDataSourceJdbc(NamedParameterJdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate;}

    @Override
    public List<Boat> showBoatData() {
        String query = "SELECT name, type, price FROM boats;";

        return jdbcTemplate.query(query, new BoatDataRowMapper());
    }

    @Override
    public List<LocalDateTime> showDays(String boatName) {
        String query = "SELECT taken_day FROM boat_availability where boat_name like :name";
        SqlParameterSource parameter = new MapSqlParameterSource()
                .addValue("name", boatName);
        return null;
    }
}
