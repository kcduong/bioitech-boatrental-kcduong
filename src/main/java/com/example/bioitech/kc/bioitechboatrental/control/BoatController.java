package com.example.bioitech.kc.bioitechboatrental.control;


import com.example.bioitech.kc.bioitechboatrental.data_access.BoatDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.Logger;

/**
 * Controller that handles boat viewing
 *
 */
@Controller
public class BoatController {
    private static final Logger logger = Logger.getLogger(BoatController.class.getName());
    private final BoatDataSource boatDataSource;

    @Autowired
    public BoatController(BoatDataSource boatDataSource) {

        this.boatDataSource = boatDataSource;
    }

    /**
     *  Retrieves all boat items required to load boat list
     */
    @GetMapping("/boats")
    public String getBoats(Model model) {
        model.addAttribute("boats", boatDataSource.showBoatData());
    return "boats";
    }

}
