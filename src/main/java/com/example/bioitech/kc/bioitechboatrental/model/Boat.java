package com.example.bioitech.kc.bioitechboatrental.model;

public class Boat {
    private String boatName;
    private String boatType;
    private int boatPrice;

    public String getBoatName() {
        return boatName;
    }

    public void setBoatName(String boatName) {
        this.boatName = boatName;
    }

    public String getBoatType() {
        return boatType;
    }

    public void setBoatType(String boatType) {
        this.boatType = boatType;
    }

    public int getBoatPrice() {
        return boatPrice;
    }

    public void setBoatPrice(int boatPrice) {
        this.boatPrice = boatPrice;
    }

    public Boat(String boatName, String boatType, int boatPrice) {
        this.boatName = boatName;
        this.boatType = boatType;
        this.boatPrice = boatPrice;
    }

    public Boat(){}
}
