package com.example.bioitech.kc.bioitechboatrental.data_access;

import com.example.bioitech.kc.bioitechboatrental.model.Boat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Interface for boat data sources
 * Handles all DAO processes
 */
public interface BoatDataSource {
    /**
     * Shows data on all boats available
     * @return list of boats and metadata
     */
    List<Boat> showBoatData();

    /**
     * Shows availability of boat rental days
     * @param boatName
     * @return list of days that the boat has been rented
     */
    List<LocalDateTime> showDays(String boatName);
}
