# Boat Rental Project

A web application that serves as a system to rent out boats. 

## Requirements

This application utilizes a MySQL database.  
Fill in the database credentials in `application.properties` for it to work:
```
spring.datasource.url=
spring.datasource.username=
spring.datasource.password=
```
Run schema.sql with data.sql for dummy boat data, or toggle `spring.datasource.initialization-mode=always`




## Built With

* [Gradle](https://gradle.org/) - Dependency Management
* [Spring](https://spring.io/) - Java Web framework
* [thymeleaf](https://www.thymeleaf.org) - templating framework